#ifndef GD_H
#define GD_H



#include "SFML/Graphics.hpp"
//dimensions in 2D that are whole numbers
struct Vec2i
{
	int x, y;
};

//dimensions in 2D that are floating point numbers
struct Vec2f
{
	float x, y;
	Vec2f():x(0),y(0){}
	Vec2f(float xx, float yy):x(xx),y(yy){}
	Vec2f(sf::Vector2f v) :x(v.x), y(v.y) {}
	~Vec2f() {}
	sf::Vector2f Vec2f::operator=(const Vec2f v) {
		return sf::Vector2f(v.x, v.y);
	}
	Vec2f Vec2f::operator=(const sf::Vector2f v) {
		return Vec2f(v.x, v.y);
	}
	bool Vec2f::operator==(const sf::Vector2f& v) {
		if ((x == v.x) && (y == v.y))
		{
			return true;
		}
		else
		{
			return false;
		}
				
	}
	bool Vec2f::operator==(const Vec2f& v) {
		if ((x == v.x) && (y == v.y))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	bool Vec2f::operator!=(const sf::Vector2f& v) {
		if ((x == v.x) && (y == v.y))
		{
			return false;
		}
		return true;
	}
	bool Vec2f::operator!=(const Vec2f& v) {
		if ((x == v.x) && (y == v.y))
		{
			return false;
		}
		return true;
	}
	Vec2f Vec2f::operator*(const Vec2f l)
	{
		return Vec2f(l.x*x, l.y*y);
	}
	Vec2f Vec2f::operator +(const Vec2f l)
	{
		return Vec2f(l.x+x, l.y+y);
	}
	Vec2f Vec2f::operator*(const float l)
	{
		x= l*x;
		y= l*y;
		return Vec2f(x, y);
	}
	Vec2f Vec2f::operator+=(const Vec2f l)
	{
		return Vec2f{ l.x + x, l.y + y };
	}
	sf::Vector2f Vec2f::ToVector2f() {
		return sf::Vector2f(x, y);
	}

};

const float PI = 3.14159265358979323846f;
inline float Deg2Rad(float deg) {
	return deg * (PI / 180.f);
}

//a namespace is just another type of box with a label where we can group things together
//GDC="GameDataConstants" - a box for all your magic numbers
//especially those that a designer might need to tweak to balance the game
namespace GDC
{
	const Vec2i SCREEN_RES{ 1200,800 };	//desired resolution
	const char ESCAPE_KEY{ 27 };		//ASCII code
	const char ENTER_KEY{ 13 };
	const char SPACEBAR{ 32 };
	const sf::Uint32 ASCII_RANGE{ 127 };//google an ASCII table, after 127 we don't care
	const float PLAY_SPIN_SPD{ 200.f };	//how fast to spin
	const float WALL_WDITH{ 0.1f };		//the walls skirt the edge of the screen
	const float GUN_LENGTH{ 70.f };		//from the centre to the end of the barrel
	const float BALL_SPEED{ 400.f };	//ball speed in units per sec
	const float FIRE_DELAY{ 0.25f };	//stop on firing for this time in secs
	const float CANNON_ROT_OFFSET{ 153.f };	//when rotating the cannon, the barrel points at a specific angle (rotate anti-clockwise from horizontal)
	const float BALL_RADIUS{ 20.f };
}

//keep important shared information "GameData" in one accessible object and pass it around
//note - structs are like classes but everything is always public, so best kept just for data
struct GD
{
	sf::Font font;				//a shared font to use everywhere
	sf::RenderWindow *pWindow;	//can't render anything without this
	std::string playerName;			//the player's name is needed by different objects
};
#endif // !GD_H