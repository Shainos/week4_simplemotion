#include "Ball.h"
void Ball::SpawnBall(float deg)
{
	//add ball spawning code here
	spawned = true;

	SetPos(Vec2f{ cos(Deg2Rad(deg)) * GDC::GUN_LENGTH + GDC::SCREEN_RES.x / 2.f , sin(Deg2Rad(deg)) * GDC::GUN_LENGTH + GDC::SCREEN_RES.y / 2.f });

	SetVel(Vec2f{ cos(Deg2Rad(deg)) * GDC::BALL_SPEED, sin(Deg2Rad(deg)) * GDC::BALL_SPEED });
}

bool Ball::CheckSpawned()
{
	//checks if the ball has been spawned before
	return spawned;
}