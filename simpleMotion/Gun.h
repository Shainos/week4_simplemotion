#ifndef GUN_H
#define GUN_H

#include "GameObj.h"
#include "Ball.h"
class Application;
//cannon spins around and can fire one cannon ball
class Gun : public GameObj
{
public:
	Gun()
		:GameObj(), mWaitSecs(0)
	{}
	void Update(Ball& mBall);
private:
	float mWaitSecs;	//delay after firing beforeo you can move/fire
};
#endif // !GUN_H