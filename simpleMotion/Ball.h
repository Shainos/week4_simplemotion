//#pragma once
#ifndef BALL_H
#define BALL_H
#include "GameObj.h"
//ball class
class Ball : public GameObj
{
public:
	Ball()
		:GameObj(), spawned(false)
	{}
	void SpawnBall(float deg);
	bool CheckSpawned();
private:
	bool spawned;

};
#endif // !BALL_H