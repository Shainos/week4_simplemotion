#ifndef PLAYMODE_H
#define PLAYMODE_H

#include "GameObj.h"
#include "Wall.h"
#include "Ball.h"
#include "Gun.h"

class Game;
//in this mode the player can spin the cannon, like a game's playable part
//similar structure to the NameMode, imagine more of these objects for each
//significant part of the game, all structured the same way
class PlayMode
{
public:
	PlayMode() : mpGame(nullptr) {}
	void Init(Game*);
	void Update();
	void Render();
private:
	Game *mpGame; //for communication

	sf::Texture mCannonTex;			//cannon and ball
	sf::Texture mWallTex;			//walls
	Wall mWalls[Wall::MAX_WALLS];	//four walls
	Gun mGun;		//cannon
	Ball mBall;
};
#endif // !PLAYMODE_H