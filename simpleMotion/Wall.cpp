#include "Wall.h"
void Wall::Update(Ball& mBall)
{
	//add ball collision code here
	switch (mType)
	{
	case Wall::LEFT:
		if (GetPos().x + (GDC::WALL_WDITH * GDC::SCREEN_RES.x) >= mBall.GetPos().x - GDC::BALL_RADIUS)
			mBall.SetVel(Vec2f(-mBall.GetVel().x, mBall.GetVel().y));
		break;
	case Wall::RIGHT:
		if (GetPos().x <= mBall.GetPos().x + GDC::BALL_RADIUS)
			mBall.SetVel(Vec2f(-mBall.GetVel().x, mBall.GetVel().y));
		break;
	case Wall::TOP:
		if (GetPos().y + (GDC::WALL_WDITH * GDC::SCREEN_RES.x) >= mBall.GetPos().y + GDC::BALL_RADIUS)
			mBall.SetVel(Vec2f(mBall.GetVel().x, -mBall.GetVel().y));
		break;
	case Wall::BOTTOM:
		if (GetPos().y <= mBall.GetPos().y + GDC::BALL_RADIUS)
			mBall.SetVel(Vec2f(mBall.GetVel().x, -mBall.GetVel().y));
		break;
	default:
		break;
	}
}